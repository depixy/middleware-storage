## [2.1.2](https://gitlab.com/depixy/middleware-storage/compare/v2.1.1...v2.1.2) (2020-12-15)


### Bug Fixes

* add region option ([9d0e918](https://gitlab.com/depixy/middleware-storage/commit/9d0e9185c6d9fbc50d530d21e587d0731faa42b8))
* option types ([5d56f79](https://gitlab.com/depixy/middleware-storage/commit/5d56f79a40231ec62a15a4efca014d4a95465485))

## [2.1.1](https://gitlab.com/depixy/middleware-storage/compare/v2.1.0...v2.1.1) (2020-12-15)


### Bug Fixes

* change option name ([02fc75e](https://gitlab.com/depixy/middleware-storage/commit/02fc75e95b0ea8e6af8dfac47403384de634c01d))

# [2.1.0](https://gitlab.com/depixy/middleware-storage/compare/v2.0.0...v2.1.0) (2020-12-01)


### Features

* add temp storage ([71dff7e](https://gitlab.com/depixy/middleware-storage/commit/71dff7ec95fe090383eef0ab625fc29ce93e4642))

# [2.0.0](https://gitlab.com/depixy/middleware-storage/compare/v1.0.1...v2.0.0) (2020-11-30)


### Bug Fixes

* remove useless options ([aa0c900](https://gitlab.com/depixy/middleware-storage/commit/aa0c900813dece4e273c2765cce1a1396ce1e750))
* update dependencies ([2f48ef9](https://gitlab.com/depixy/middleware-storage/commit/2f48ef9ec955c417f2f93c36445c47942f390c5f))


### Code Refactoring

* new middleware interface ([04b0d97](https://gitlab.com/depixy/middleware-storage/commit/04b0d9763487d8e261b0dd33b45e68922a98e8a1))


### BREAKING CHANGES

* both local and s3 are now included in the package

## [1.0.1](https://gitlab.com/depixy/middleware-storage/compare/v1.0.0...v1.0.1) (2020-07-23)


### Bug Fixes

* initial storage on create ([fcb8cdc](https://gitlab.com/depixy/middleware-storage/commit/fcb8cdc8699e103ec8743af3bf348c6e88b1c7b2))

# 1.0.0 (2020-07-21)


### Features

* initial commit ([e4e7458](https://gitlab.com/depixy/middleware-storage/commit/e4e7458ab05f43475c4749672b64c4641c13534b))
