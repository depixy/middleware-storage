import { Client, ClientOptions } from "minio";
import { Readable } from "stream";

import { Metadata, Storage } from "./type";

export class S3Storage implements Storage {
  protected client: Client;
  protected bucket: string;

  constructor(bucket: string, option: ClientOptions) {
    this.client = new Client(option);
    this.bucket = bucket;
  }

  async save(
    key: string,
    stream: Readable,
    metadata: Record<string, any> = {}
  ): Promise<void> {
    await this.client.putObject(this.bucket, key, stream, metadata);
  }

  async load(key: string): Promise<Readable> {
    return this.client.getObject(this.bucket, key);
  }

  async exist(key: string): Promise<boolean> {
    try {
      await this.metadata(key);
      return true;
    } catch (e) {
      return false;
    }
  }

  async metadata(key: string): Promise<Metadata> {
    const stat = await this.client.statObject(this.bucket, key);
    return {
      metadata: stat.metaData,
      etag: stat.etag,
      lastModified: stat.lastModified
    };
  }

  async delete(key: string): Promise<void> {
    await this.client.removeObject(this.bucket, key);
  }

  async deleteMany(keys: string[]): Promise<void> {
    await this.client.removeObjects(this.bucket, keys);
  }
}
