import type { Middleware } from "koa";

export * from "./type";

import {
  LocalStorageOption,
  S3StorageOption,
  Storage,
  StorageOption
} from "./type";

import { S3Storage } from "./s3";
import { LocalStorage } from "./local";

const createStorage = (opt: S3StorageOption | LocalStorageOption): Storage => {
  switch (opt.type) {
    case "s3": {
      const {
        bucket,
        endPoint,
        accessKey,
        secretKey,
        port,
        ssl: useSSL,
        region
      } = opt.option;
      return new S3Storage(bucket, {
        endPoint,
        accessKey,
        secretKey,
        port,
        useSSL,
        region
      });
    }
    case "local": {
      const { baseDir } = opt.option;
      return new LocalStorage(baseDir);
    }
    default:
      throw new Error("Unknown storage type");
  }
};

export const middleware = async (
  option: StorageOption
): Promise<Middleware> => {
  const storage = createStorage(option.storage);
  const temp = createStorage(option.temp);
  return async (ctx, next) => {
    ctx.storage = storage;
    ctx.temp = temp;
    await next();
  };
};

export default middleware;
