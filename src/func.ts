import { Readable, Writable } from "stream";

export const toPromise = async <T extends Readable, U extends Writable>(
  input: T,
  output: U
): Promise<void> => {
  let ended = false;
  function end(): boolean {
    if (!ended) {
      ended = true;
      return true;
    }
    return false;
  }

  return new Promise((resolve, reject) => {
    function niceEnding(): void {
      if (end()) {
        resolve();
      }
    }

    function errorEnding(error: Error): void {
      if (end()) {
        reject(error);
      }
    }
    input.pipe(output);
    input.on("error", errorEnding);
    output.on("finish", niceEnding);
    output.on("end", niceEnding);
    output.on("error", errorEnding);
  });
};
