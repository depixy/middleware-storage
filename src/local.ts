import { Readable } from "stream";
import fs from "fs";
import fsp from "fs/promises";
import path from "path";
import crypto from "crypto";

import { Metadata, Storage } from "./type";
import { toPromise } from "./func";

export class LocalStorage implements Storage {
  protected baseDir: string;

  constructor(baseDir: string) {
    const stat = fs.lstatSync(baseDir);
    this.baseDir = baseDir;
    if (!stat.isDirectory()) {
      throw new Error(`${baseDir} is not a directory.`);
    }
  }

  normalizeKey(key: string): string {
    const normalized = path.normalize(key);
    return normalized.startsWith("/") ? normalized.substr(1) : normalized;
  }

  resolveKey(key: string): string {
    return path.join(this.baseDir, this.normalizeKey(key));
  }

  resolveMetadata(key: string): string {
    const resolve = this.resolveKey(key);
    return path.join(
      path.dirname(resolve),
      path.basename(resolve, path.extname(resolve)) + ".metadata.json"
    );
  }

  resolveHash(key: string): string {
    const resolve = this.resolveKey(key);
    return path.join(
      path.dirname(resolve),
      path.basename(resolve, path.extname(resolve)) + ".metadata.hash"
    );
  }

  async save(
    key: string,
    stream: Readable,
    metadata: Record<string, any> = {}
  ): Promise<void> {
    const filePath = this.resolveKey(key);
    const metadataPath = this.resolveMetadata(key);
    const output = fs.createWriteStream(filePath);
    const writeFile = toPromise(stream, output);
    const writeMetadata = fsp.writeFile(
      metadataPath,
      JSON.stringify(metadata),
      { encoding: "utf8" }
    );
    await Promise.all([writeFile, writeMetadata]);

    const hashPath = this.resolveHash(key);
    const file = await fsp.readFile(filePath);
    const sum = crypto.createHash("sha1");
    sum.update(file);
    const hash = sum.digest("hex");
    await fsp.writeFile(hashPath, hash, { encoding: "utf8" });
  }

  async load(key: string): Promise<Readable> {
    const filePath = this.resolveKey(key);
    return fs.createReadStream(filePath);
  }

  async exist(key: string): Promise<boolean> {
    const filePath = this.resolveKey(key);
    try {
      await fsp.lstat(filePath);
      return true;
    } catch (e) {
      return false;
    }
  }

  async metadata(key: string): Promise<Metadata> {
    const filePath = this.resolveKey(key);
    const metadataPath = this.resolveMetadata(key);
    const hashPath = this.resolveHash(key);
    const stat = await fsp.lstat(filePath);
    const [metadataStr, hash] = await Promise.all([
      fsp.readFile(metadataPath, "utf8"),
      fsp.readFile(hashPath, "utf8")
    ]);
    const metadata = JSON.parse(metadataStr);
    return {
      metadata,
      etag: hash,
      lastModified: stat.mtime
    };
  }

  async delete(key: string): Promise<void> {
    const filePath = this.resolveKey(key);
    const metadataPath = this.resolveMetadata(key);
    const hashPath = this.resolveHash(key);
    await Promise.all([
      fsp.unlink(filePath),
      fsp.unlink(metadataPath),
      fsp.unlink(hashPath)
    ]);
  }

  async deleteMany(keys: string[]): Promise<void> {
    await Promise.all(keys.map(this.delete));
  }
}
