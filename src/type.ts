import { Readable } from "stream";

declare module "koa" {
  interface ExtendableContext {
    storage: Storage;
    temp: Storage;
  }
}

export interface Metadata {
  metadata: Record<string, any>;
  etag: string;
  lastModified: Date;
}

export interface Storage {
  save(
    key: string,
    stream: Readable,
    metadata?: Record<string, any>
  ): Promise<void>;
  load(key: string): Promise<Readable>;
  metadata(key: string): Promise<Metadata>;
  exist(key: string): Promise<boolean>;
  delete(key: string): Promise<void>;
  deleteMany(keys: string[]): Promise<void>;
}

export type StorageOption = {
  storage: S3StorageOption | LocalStorageOption;
  temp: S3StorageOption | LocalStorageOption;
};

export interface S3StorageOption {
  type: "s3";
  option: {
    endPoint: string;
    ssl?: boolean;
    port?: number;
    bucket: string;
    accessKey: string;
    secretKey: string;
    region?: string;
  };
}

export interface LocalStorageOption {
  type: "local";
  option: {
    baseDir: string;
  };
}
